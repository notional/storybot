package com.notional.storybot.listener

annotation class TestMarker

@TestMarker
annotation class Issue(vararg val value: String)