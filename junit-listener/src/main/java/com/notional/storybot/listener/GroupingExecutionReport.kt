package com.notional.storybot.listener

import org.junit.platform.engine.TestExecutionResult

class GroupingExecutionReport(private val groupByName: String, val executions: List<TestExecution>) {
    data class GroupedExecution(val displayName: String, val uniqueId: String, val status: TestExecutionResult.Status)

    val groupedExecutions: Map<String, List<GroupedExecution>>

    init {
        groupedExecutions = executions.flatMap { execution ->
            execution.markers.getOrDefault(groupByName, emptyList()).map { markerValue -> markerValue to execution }
        }.groupBy(
                keySelector = { it.first.toString() },
                valueTransform = { GroupedExecution(it.second.displayName, it.second.uniqueId, it.second.status) }
        )
    }
}
