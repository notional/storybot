package com.notional.storybot.listener

import com.fasterxml.jackson.annotation.JsonIgnore
import org.apache.commons.lang3.StringUtils
import org.junit.platform.engine.TestExecutionResult
import org.junit.platform.engine.support.descriptor.MethodSource
import org.junit.platform.launcher.TestExecutionListener
import org.junit.platform.launcher.TestIdentifier
import org.junit.platform.launcher.TestPlan
import java.nio.file.Paths

class StorybotListener : TestExecutionListener {
    override fun testPlanExecutionStarted(testPlan: TestPlan?) {
    }

    override fun executionStarted(testIdentifier: TestIdentifier) {

    }

    override fun executionFinished(testIdentifier: TestIdentifier, testExecutionResult: TestExecutionResult) {
        if (!testIdentifier.isTest) {
            return
        }
        testIdentifier.source.ifPresent { testSource ->
            val markers = when (testSource) {
                is MethodSource -> findMarkers(testSource)
                else -> emptyMap()
            }
            TestRepostiory.addExecution(TestExecution(testIdentifier, markers, testExecutionResult.status))
        }
    }

    override fun testPlanExecutionFinished(testPlan: TestPlan?) {
        val baseDir = when {
            System.getProperty("basedir").isNotEmpty() -> Paths.get(System.getProperty("basedir"), "target", "storybot-report")
            else -> null
        }
        if (baseDir != null) {
            val baseDirName = Paths.get(System.getProperty("basedir")).fileName.toString()
            TestReportWriter(baseDir, listOf("storybot.json", "$baseDirName.storybot.json")).write(TestRepostiory.executions.values.toList())
        }
    }

    private fun findMarkers(testSource: MethodSource): Map<String, List<Any>> {
        val clazz = Class.forName(testSource.className)
        val method = clazz.getMethod(testSource.methodName)
        val markers = method.annotations
                .filter { it.annotationClass.java.isAnnotationPresent(TestMarker::class.java) }
                .map { annotation ->
                    val value = annotation.annotationClass.java.getMethod("value").invoke(annotation)

                    val collection = when (value) {
                        is Array<*> -> value.toList()
                        is Iterable<*> -> value.toList()
                        else -> listOf(value)
                    }
                    val simpleName = annotation.annotationClass.java.simpleName!!
                    simpleName to collection as List<Any>
                }.toMap()
        return markers
    }
}

object TestRepostiory {
    fun addExecution(testExecution: TestExecution) = executions.put(testExecution.uniqueId, testExecution)

    val executions = mutableMapOf<String, TestExecution>()
}

data class TestExecution(
        val uniqueId: String,
        val testName: String,
        val markers: Map<String, List<Any>>,
        val status: TestExecutionResult.Status
) {
    constructor(identifier: TestIdentifier,
                markers: Map<String, List<Any>>,
                status: TestExecutionResult.Status
    ) : this(identifier.uniqueId, identifier.displayName, markers, status)

    val displayName = StringUtils.splitByCharacterTypeCamelCase(testName.removeSuffix("()")).joinToString(" ")

}