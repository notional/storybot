package com.notional.storybot.listener

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.util.ClassUtil
import java.nio.file.Path

class TestReportWriter(val outputDir: Path, val fileNames:List<String> = listOf("storybot.json")) {
    fun write(executions: List<TestExecution>) {
        outputDir.toFile().mkdirs()

        val groupedReport = GroupingExecutionReport("Issue", executions);
        fileNames.forEach {
            val file = outputDir.resolve(it).toFile()
            ObjectMapper().writerWithDefaultPrettyPrinter().writeValue(file, groupedReport)
        }
    }
}

