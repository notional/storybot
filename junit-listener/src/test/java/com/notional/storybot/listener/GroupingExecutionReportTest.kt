package com.notional.storybot.listener

import com.winterbe.expekt.expect
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test
import org.junit.platform.engine.TestExecutionResult
import org.junit.platform.launcher.TestIdentifier

internal class GroupingExecutionReportTest {
    @Test
    fun groupsTestsByIssue() {
        val executions = listOf(TestExecution(
                "test-1",
                "aTestShouldPass()",
                mapOf("Issue" to listOf("JIRA-1")),
                TestExecutionResult.Status.SUCCESSFUL
        ),
                TestExecution(
                        "test-1",
                        "aTestShouldPass()",
                        mapOf("Issue" to listOf("JIRA-1", "JIRA-2")),
                        TestExecutionResult.Status.SUCCESSFUL)
        )
        val report = GroupingExecutionReport("Issue", executions)
        expect(report.groupedExecutions["JIRA-1"]).to.have.size(2)
        expect(report.groupedExecutions["JIRA-2"]).to.have.size(1)
    }
}