import { Component, OnInit } from '@angular/core';

const theIssues = {
  groupedExecutions: {
    'JIRA-123': [
      {
        displayName: 'tests Can Have Multiple Issues',
        uniqueId: '[engine:junit-jupiter]/[class:com.notional.storybot.sample.SampleTest]/[method:testsCanHaveMultipleIssues()]',
        status: 'SUCCESSFUL'
      },
      {
        displayName: 'test Names Can Be Declared Like This',
        uniqueId: '[engine:junit-jupiter]/[class:com.notional.storybot.sample.SampleTest]/[method:testNamesCanBeDeclaredLikeThis()]',
        status: 'SUCCESSFUL'
      }
    ],
    'JIRA-456': [
      {
        displayName: 'tests Can Have Multiple Issues',
        uniqueId: '[engine:junit-jupiter]/[class:com.notional.storybot.sample.SampleTest]/[method:testsCanHaveMultipleIssues()]',
        status: 'SUCCESSFUL'
      }
    ]
  },
  executions: [
    {
      displayName: 'tests Can Have Multiple Issues',
      uniqueId: '[engine:junit-jupiter]/[class:com.notional.storybot.sample.SampleTest]/[method:testsCanHaveMultipleIssues()]',
      testName: 'testsCanHaveMultipleIssues()',
      markers: {
        Issue: [
          'JIRA-123',
          'JIRA-456'
        ]
      },
      status: 'SUCCESSFUL'
    },
    {
      displayName: 'test Names Can Be Declared Like This',
      uniqueId: '[engine:junit-jupiter]/[class:com.notional.storybot.sample.SampleTest]/[method:testNamesCanBeDeclaredLikeThis()]',
      testName: 'testNamesCanBeDeclaredLikeThis()',
      markers: {
        Issue: [
          'JIRA-123'
        ]
      },
      status: 'SUCCESSFUL'
    }
  ]
};

@Component({
  selector: 'app-issue-table',
  templateUrl: './issue-table.component.html',
  styleUrls: ['./issue-table.component.scss']
})
export class IssueTableComponent  {

  issues = theIssues;
}

