package com.notional.storybot.sample

import com.notional.storybot.listener.Issue
import org.junit.jupiter.api.Test

class SampleTest {

    @Test
    @Issue("JIRA-123")
    fun testNamesCanBeDeclaredLikeThis() {
    }

    @Test
    @Issue("JIRA-123", "JIRA-456")
    fun testsCanHaveMultipleIssues() {
    }

}
